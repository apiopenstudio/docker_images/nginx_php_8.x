ARG UBUNTU_VERSION=22.04

FROM ubuntu:${UBUNTU_VERSION}

ARG PHP
ARG NOW
ARG TAG
ARG CI_IMAGE_NAME=ci-nginx-php-UNKNOWN

ENV DOCKER_CMD="service php${PHP}-fpm start && ln -s /run/php/php${PHP}-fpm.sock /run/php/php-fpm.sock && service nginx start"
ENV DEBIAN_FRONTEND noninteractive

LABEL org.opencontainers.image.authors="https://www.apiopenstudio.com"
LABEL org.opencontainers.image.description="Nginx and PHP${PHP} with ApiOpenStudio application server dependencies for CI testing. Packaged by ApiOpenStudio."
LABEL org.opencontainers.image.source="https://hub.docker.com/repository/docker/nginx_php_8.x"
LABEL org.opencontainers.image.title="ApiOpenStudio CI Nginx PHP${PHP}"
LABEL org.opencontainers.image.vendor="Naala Pty Ltd"
LABEL org.opencontainers.image.version="${TAG}"
LABEL org.opencontainers.image.created="${NOW}"

RUN apt-get update \
    && apt-get install -yq software-properties-common \
    && add-apt-repository -y ppa:ondrej/php

RUN apt-get install -yq apt-transport-https \
    ca-certificates \
    curl \
    git \
    lsb-release \
    mariadb-client \
    nginx \
    openssh-client \
    php${PHP} \
    php${PHP}-cli \
    php${PHP}-common \
    php${PHP}-ctype \
    php${PHP}-curl \
    php${PHP}-dom \
    php${PHP}-fileinfo \
    php${PHP}-fpm \
    php${PHP}-iconv \
    php${PHP}-mbstring \
    php${PHP}-mysqli \
    php${PHP}-phar \
    php${PHP}-simplexml \
    php${PHP}-tokenizer \
    php${PHP}-xml \
    php${PHP}-xmlwriter \
    unzip \
    zip \
    && rm -rf /var/lib/apt/lists/*

# Install Composer
RUN curl -sS https://getcomposer.org/installer | \
    php -- --install-dir=/usr/bin --filename=composer && chmod +x /usr/bin/composer

# Nginx config
RUN mkdir /www
COPY config/nginx/nginx.conf /etc/nginx/nginx.conf
COPY config/nginx/sites-available/default /etc/nginx/sites-available/default

CMD ["/bin/bash", "-c", "$DOCKER_CMD"]
